const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete');

const CourseSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },

    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users',
        required: true
    },

    category: {
        type: String,
        required: true
    },

    image: {
        type: String,
        default: null,
        required: false,
        get: getImage
    },

    description: {
        type: String,
        required: false
    },

    lesson: {
        type: Number,
        required: false,
        default: 0
    },

    material: {
        type: Number,
        required: false,
        default: 0
    },

    enrolled: {
        type: Number,
        required: false,
        default: 0
    },
    isPublished: {
        type: Boolean,
        required: true,
        default: false
    },
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    },
    versionKey: false,
    toJSON: { getters: true }
});

function getImage(image) {
    return 'https://lektur.kuyrek.com/imgCourses/' + image
}

CourseSchema.plugin(mongoose_delete, { overrideMethods: 'all' });

module.exports = courses = mongoose.model('courses', CourseSchema, 'courses');