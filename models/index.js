const path = require('path');
require('dotenv').config({
  path: `./environments/.env.${process.env.NODE_ENV}`
})

const mongoose = require('mongoose');

const uri = process.env.MONGO_URI

mongoose.connect(uri, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true
})

const users = require('./users.js')
const courses = require('./courses.js')
const lessons = require('./lessons.js')
const tests = require('./tests.js')
const enrolls = require('./enrolls.js')
const studentLessons = require('./studentLessons.js')
const options = require('./options.js')
const questions = require('./questions.js');
const studentTests = require('./studentTests.js')
const studentOptions = require('./studentOptions.js')

module.exports = { 
    users, 
    courses, 
    lessons,  
    enrolls,
    studentLessons,
    options,
    tests,
    questions,
    studentTests,
    studentOptions
};
