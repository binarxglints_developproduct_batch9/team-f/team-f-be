const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete');

const OptionsSchema = new mongoose.Schema({
    question: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'questions',
        required: true
    },

    option: {
        type: String,
        required: true
    },

    answer: {
        type: Boolean,
        default: false,
        required: true,
    },


}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    },
    versionKey: false,
    toJSON: { getters: true }
});


OptionsSchema.plugin(mongoose_delete, { overrideMethods: 'all' });

module.exports = options = mongoose.model('options', OptionsSchema, 'options');