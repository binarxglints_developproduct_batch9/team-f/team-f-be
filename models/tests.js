const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete');

const TestSchema = new mongoose.Schema({
    course: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'courses',
        required: true
    },

    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users',
        required: true
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    },

    versionKey: false
});

TestSchema.plugin(mongoose_delete, { overrideMethods: 'all' });

module.exports = tests = mongoose.model('tests', TestSchema, 'tests');