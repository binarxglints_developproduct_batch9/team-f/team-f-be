const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete');

const LessonsSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users',
        required: true
    },

    course: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'courses',
        required: true
    },

    title: {
        type: String,
        required: true
    },

    video: {
        type: String,
        required: true,
        get: getVideo
    },

    material: {
        type: mongoose.Schema.Types.Array,
        get: getMaterial,
        required: false
    },

    description: {
        type: String,
        required: false
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    },
    versionKey: false,
    toJSON: { getters: true }
});

function getVideo(video) {
    return 'https://lektur.kuyrek.com/lessonAssets/' + video
}

function getMaterial(value) {
    let obj = []
    for (let key of value) {obj.push(`https://lektur.kuyrek.com/lessonAssets/${key}`)}
    value = obj
    return value
}

LessonsSchema.plugin(mongoose_delete, { overrideMethods: 'all' });

module.exports = lessons = mongoose.model('lessons', LessonsSchema, 'lessons');