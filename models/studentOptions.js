const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete');

const StudentOptionsSchema = new mongoose.Schema({

    test: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'tests',
        required: true
    },

    question: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'questions',
        required: true
    },

    answer: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'options',
        required: true
    },

    studentTest: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'studentTests',
        required: true
    },

    result: {
        type: Boolean,
        required: false,
        default: null
    }

}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    },
    versionKey: false,
    toJSON: { getters: true }
});

StudentOptionsSchema.plugin(mongoose_delete, { overrideMethods: 'all' });

module.exports = studentOptions = mongoose.model('studentOptions', StudentOptionsSchema, 'studentOptions');