const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete');

const EnrollsSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.Mixed,
        required: true
    },

    course: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'courses',
        required: true
    },

    teacher: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users',
        required: true
    },

    status: {
        type: String,
        required: true,
        default: 'pending'
    },

    totalLesson: {
        type: Number,
        required: false,
        default: null
    },

    totalLessonCompleted: {
        type: Number,
        required: false,
        default: 0
    },

    score: {
        type: Number,
        required: false,
        default: null
    },

}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    },
    versionKey: false,
    toJSON: { getters: true }
});

EnrollsSchema.plugin(mongoose_delete, { overrideMethods: 'all' });

module.exports = enrolls = mongoose.model('enrolls', EnrollsSchema, 'enrolls');