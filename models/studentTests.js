const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete');

const StudentTestsSchema = new mongoose.Schema({

    enroll: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'enrolls',
        required: true
    },

    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users',
        required: true
    },

    test: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'tests',
        required: true
    },


    totalQuestion: {
        type: Number,
        required: false,
        default: null
    },

    totalCorrect: {
        type: Number,
        required: false,
        default: null
    },

    score: {
        type: Number,
        required: false,
        default: 0
    },

    completedAt: {
        type: Date,
        required: false,
        default: null
    }

}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    },
    versionKey: false,
    toJSON: { getters: true }
});

StudentTestsSchema.plugin(mongoose_delete, { overrideMethods: 'all' });

module.exports = studentTests = mongoose.model('studentTests', StudentTestsSchema, 'studentTests');