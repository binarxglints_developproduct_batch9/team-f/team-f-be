const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete');

const StudentLessonsSchema = new mongoose.Schema({

    enroll: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'enrolls',
        required: true
    },

    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users',
        required: true
    },

    course: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'courses',
        required: true
    },

    lesson: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'lessons',
        required: true
    },

    isCompleted: {
        type: Boolean,
        required: false,
        default: false
    },

    isPermission: {
        type: Boolean,
        required: false,
        default: false
    },

}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    },
    versionKey: false,
    toJSON: { getters: true }
});

StudentLessonsSchema.plugin(mongoose_delete, { overrideMethods: 'all' });

module.exports = studentLessons = mongoose.model('studentLessons', StudentLessonsSchema, 'studentLessons');