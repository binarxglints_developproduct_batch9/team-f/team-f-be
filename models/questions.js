const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete');

const QuestionSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users',
        required: true
    },

    course: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'courses',
        required: true
    },

    test: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'tests',
        required: true
    },

    question: {
        type: String,
        required: true
    },

    remark: {
        type: String,
        required: true
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    },

    versionKey: false
});

QuestionSchema.plugin(mongoose_delete, { overrideMethods: 'all' });

module.exports = questions = mongoose.model('questions', QuestionSchema, 'questions');