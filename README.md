# WELCOME TO LEKTUR API

# Team Lektur [Back-End]

This project is part of Lektur with other developers:

- Backend Team:
  Khai, Sherary, Tiur
- Frontend Team:
  Aldhy, Gary, Iqbal
- React Native
  Hendrick, Reza

# About this project

A Learning Management System that let students access materials online, track learning progress, and do assessments on students taking the course.

# Please read! [For Developer]

Please read this readme file first before you start coding.

## This starter project

This starter project already contains some basic library like:

- Express
- Already installed Mongoose
- Already installed Mongodb
- Already installed Bycript
- Already installed Nodemon
- Already installed Passport
- Already installed Multer

## How to run the project

You need to install required dependencies (libraries) by typing in the terminal

```bash
npm install
```

Then you can run this project by:

- Using node
  ```bash
  npm start
  ```
- Using nodemon

---

## Working the feature

1. To run the database you can create a database in your local and run command sequelize db:migrate.
2. Use a git flow methodologies to create a feature,
   Allowed branches are
   - master / main
   - feature with `feature-FEATURE_NAME` name scheme like `feature-authentication`, `feature-courses`
   - you can pull from `development`

---

## Branching

Please create your own branch like: `feature-FEATURE_NAME`, also work per feature.  
The step is:

- clone this repo
- git checkout `development`
- git branch `feature-FEATURE_NAME`
- git checkout `feature-FEATURE_NAME`

For example: creating authentication like login.  
make sure you are on `feature-authentication` branch!

- git branch `feature-authentication`
- git checkout `feature-authentication`

start coding...
if its all good, push.

- git add .
- git commit -m "Adding auth feature"
- git push origin `feature-authentication`

after create an auth feature then you want to create other feature, back to your `development` branch.

- git checkout `development`
- git branch `feature-user`
- git checkout `feature-user`

start coding...
if ok, push againn with:

- git add .
- git commit -m "Add feature user"
- git push origin `feature-user`

please back to `development` if you want to create other feature

## Descriptive commit message

Please use descriptive commit message for example:

- git commit -m "Fixed bugs when login"
- git commit -m "Add login controller"
- git commit -m "Refactor user validator"

DONT DO THIS:

- git commit -m "r"
- git commit -m "test"
- git commit -m "push 1"
