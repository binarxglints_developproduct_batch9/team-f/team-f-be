const { courses } = require('../models');
const { ObjectId } = require('mongodb');
const fs = require('fs')

class CourseController {

    async getAll(req, res) {

        try {
            const allCourses = await courses.find({
                isPublished : true
            })
            .sort({'created_at': -1})
            .populate('user', 'name _id') //TO DISPLAY JOIN TABLE WITH USER

            // RESULT
            res.json({
                status: 'Success get all courses!',
                data: allCourses
            })
        } catch (error) {
            console.error(error);
        }
        
    }

    async getOne(req, res) {
        courses.findOne({
            _id: req.params.id,
            isPublished : true
        }).populate('user', 'name _id')
        .then(result => {
            res.json({
                status: 'Success get course data!',
                data: result
            })
        })
    }

    async searchByTeacher(req, res) {
        courses.find({
            user: new ObjectId(req.user._id),
            isPublished : true
        }).populate('user', 'name _id')
        .then(result => {
            res.json({
                message: 'Success searching course by teacher',
                data: result
            })
        })
    }

    async searchByCategory(req, res) {
        courses.find({
            isPublished : true,
            category: {
                $regex: req.query.key,
                $options: 'i'
            }
        }).populate('user', 'name _id')
        .then(result => {
            res.json({
                message: 'Success searching course by category',
                data: result
            })
        })
    }

    async searchByTitle(req, res) {
        courses.find({
            isPublished : true,
            title: {
                $regex: req.query.key,
                $options: 'i'
            }
        }).populate('user', 'name _id')
        .then(result => {
            res.json({
                message: 'Success searching course by title',
                data: result
            })
        })
    }

    async create(req, res) {

        try {
            // CREATE NEW COURSE
            const createNewCourse = await courses.create({
                user: req.user._id,
                image: req.file === undefined ? '' : req.file.filename,
                title: req.body.title,
                category: req.body.category,
                description: req.body.description,   
            })

            res.json({
                status: 'Success create course!',
                data: createNewCourse
            })

        } catch (error) {
            console.error(error);
        }
    }

    async publishCourse (req, res) {
        try {
            const publishCourse = await courses.findOneAndUpdate({
                _id: req.params.id
            }, { isPublished: true }, { new: true })

            res.json({
                'status': 'Success publish the course!',
                'data': publishCourse
            })
        } catch (error) {
            console.error(error);
        }
    }

    async getUnpublished(req, res) {
        courses.find({
            user: new ObjectId(req.user._id),
            isPublished : false
        }).populate('user', 'name _id')
        .then(result => {
            res.json({
                message: 'Success searching course by teacher',
                data: result
            })
        })
    }

    async update(req, res) {
        try {
            let updateObj = {}

            // GET DETAIL
            const getId = await courses.findOne({
                _id: req.params.id
            })

            // DELETE IMAGE ON FILE SYSTEM
            if(req.file) {
                let trim = getId.image.slice(37)
                const path = "./public/imgCourses/" + trim
                const deleteFile = await fs.unlinkSync(path)
            }

            //SELECTIVE UPDATE
            if(req.file && req.file.filename) updateObj.image = req.file ? req.file.filename : " "
            if(req.body.title) updateObj.title = req.body.title
            if(req.body.category) updateObj.category = req.body.category
            if(req.body.description) updateObj.description = req.body.description

            const updateCourse = await courses.findOneAndUpdate({
                _id: req.params.id
            }, { $set: updateObj }, { new: true })

            res.json({
                'status': 'Success updating the course!',
                'data': updateCourse
            })
        } catch (error) {
            console.error(error);
        }
    }

    async delete(req, res) {
        try {
            // GET DETAIL
            const getId = await courses.findOne({
                _id: req.params.id
            })

            if(getId.image.includes('jpg')){
                let trim = getId.image.slice(37)
                const path = "./public/imgCourses/" + trim
                const deleteFile = await fs.unlinkSync(path)
            }

            const deleteCourse = await courses.delete({
                _id: req.params.id
            })

            res.json({
                'status': 'Success delete course!',
                data: null
            })
        } catch(error) {
            console.error(error);
        }
    }
}

module.exports = new CourseController;