// IMPORT CONTROLLER DEPENDENCIES
const path = require('path');
require('dotenv').config({
  path: `./environments/.env.${process.env.NODE_ENV}`
})

const { users } = require('../models')
const passport = require('passport')
const jwt = require('jsonwebtoken')
const key = 'lektur_secret_key'
const fs = require('fs')
const nodemailer = require('nodemailer')
const smtpTransport = require('nodemailer-smtp-transport');
const transporter = nodemailer.createTransport(smtpTransport({
  service: 'gmail',
  host: 'smtp.gmail.com',
  port: 465,
  secure: true,
  auth: {
    user: process.env.NODEMAILER_MAIL,
    pass: process.env.NODEMAILER_PASS
  },
  tls: {
    rejectUnauthorized: false
  }
}))

class UserController {

  // METHOD FOR SIGNUP
  async signup(req, res) {

    try {
      //DEFINE USER TOKEN CONTENT
      const userData = {
        _id: req.user._id,
        email: req.user.email
      };

      // CREATE TOKEN FROM USER DATA
      const token = jwt.sign({
        user: userData
      }, key)

      // SEND EMAIL SUCCESS SIGNUP
      let mailOptions = {
        from: 'lekturprojectb9@gmail.com',
        to: req.user.email,
        subject: 'Please confirm your Email account',
        html: "Hello,<br> Please Click on the link to verify your email.<br><a href="+"https://"+req.headers.host+"/verification/"+token+">Click here to verify</a>",
      }

      const sendEmail = await transporter.sendMail(mailOptions)
      .then(result => {
        console.log('Email sent');
      })
      .catch(error => {
        res.json({
          status: error
        })
      })

      // RESULT TOKEN
      res.status(200).json({
        status: 'Signup success!',
        message: "Please check email to verify your account"
      })

    } catch (error) {
      return res.status(500).json({
        message: "Internal server error",
        error: error
      })
    }
  }

  // METHOD FOR VERIFICATION
  async verification(req, res) {
    try {
      const getUserData = jwt.decode(req.params.token)
      const validate = await users.findOne({
        email: getUserData.user.email
      })
    
      if (!validate) {
        throw new Error('User not found')
      }

      const verify = await users.findOneAndUpdate({
        _id: validate._id
      }, {isVerified: true}, {new: true})

      return res.status(200).json({
        status: "Verification Success",
        data: "Please login to start learning on lektur"
      })

    } catch (e) {
      return res.status(401).json({
        status: "Error!",
        message: "Unauthorized",
      })
    }
  }

  // METHOD FOR LOGIN
  async login(req, res) {

    try {
      //DEFINE USER TOKEN CONTENT
      const userData = {
        _id: req.user._id,
        email: req.user.email
      };

      // CREATE TOKEN FROM USER DATA
      const token = jwt.sign({
        user: userData
      }, key)

      // RESULT TOKEN
      res.status(200).json({
        message: 'Login success!',
        token: token
      })

    } catch (error) {
      return res.status(500).json({
        message: "Internal server error",
        error: e
      })
    }
  }

  // METHOD TO HANDLE FAILED GOOGLE AUTH
  async failed(req,res) {
    try {
      return res.status(500).json({
        message: "Internal server error"
      })
    } catch (e) {
      return res.status(500).json({
        message: "Internal server error",
        error: e
      })
    }
  }
  
  // METHOD FOT GET ALL USER PROFILE
  async getAll(req, res) {

    try {
      const getUserProfile = await users.find({})

      res.json({
        "status": "success",
        "message": "get all user profile",
        "data": getUserProfile
      })

    } catch (error) {
      console.error(error)
    }
  }

  // METHOD FOT GET USER PROFILE
  async getUserProfile(req, res) {

    try {
      const getUserProfile = await users.findOne({
        _id: req.user._id
      })

      res.json({
        "status": "success",
        "message": "user profile",
        "data": getUserProfile
      })

    } catch (error) {
      console.error(error)
    }
  }

  // METHOD FOT GET USER PROFILE
  async updateUserProfile(req, res) {

    try {
      let updateObj = {}

      if (req.file && req.file.filename) updateObj.image = req.file ? req.file.filename : " "
      if (req.body.name) updateObj.name = req.body.name
      if (req.body.email) updateObj.email = req.body.email
      if (req.body.role) updateObj.role = req.body.role

      const updateUserProfile = await users.findOneAndUpdate({
        _id: req.user._id
      }, {
        $set: updateObj
      }, {
        new: true
      })

      res.json({
        "status": "success",
        "message": "profile updated",
        "data": updateUserProfile
      })

    } catch (error) {
      console.error(error)
    }
  }

  // METHOD FOT GET ALL USER PROFILE
  async delete(req, res) {

    try {
      const getId = await users.findOne({
        _id: req.params.id
      })

      const deleteUserProfile = await users.delete({
        _id: req.params.id
      })

      let trim = getId.image.slice(36)

      const path = "./public/imageUser/" + trim

      const deleteFile = await fs.unlinkSync(path)

      res.json({
        "status": "success",
        "message": "user deleted",
        "data": null
      })

    } catch (error) {
      console.error(error)
    }
  }
}

module.exports = new UserController