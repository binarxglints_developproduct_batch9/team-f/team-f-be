const { tests, users } = require('../models');
const { ObjectId } = require('mongodb');

class TestController {

    async getAll(req, res) {
        try {
            const allTests = await tests.find({
                user: req.user._id
            })
            .populate('course', 'title')

            res.json({
                status: 'Success get all tests!',
                data: allTests
            })
        } catch (error) {
            console.error(error);
        }
    }

    async getOne(req, res) {
        try {
            const getOneTest = await tests.find({
                _id: req.params.id
            })

            res.json({
                status: 'Success get tests data!',
                data: getOneTest
            })
        } catch (error) {
            console.error(error)
        }
    }

    async getByCourse(req, res) {
        try {
            const getTheCourse = await tests.find({
                course: new ObjectId(req.params.id)
            })

            res.json({
                status: 'Success get test by course!',
                data: getTheCourse
            })
        } catch (error) {
            console.error(error);
        }
    }

    async create(req, res) {

        try {
            const createTest = await Promise.all([
                tests.create({
                    course: req.body.course_id,
                    user: req.user._id
                })
            ])

            res.json({
                status: 'Success taking test!',
                data: createTest
            })
        } catch (error) {
            console.error(error)
        }
    }

    async delete(req, res) {
        try {
            const deleteTest = await tests.delete({
                _id: req.params.id
            })

            res.json({
                status: 'Success delete test!',
                data: null
            })
        } catch (error) {
            console.error(error);
        }
    }
}

module.exports = new TestController;