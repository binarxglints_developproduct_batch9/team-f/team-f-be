const { users, courses, tests, questions, options } = require('../models');
const { ObjectId } = require('mongodb');

class QuestionController {
    async getAll(req, res) {
        try {
            const allQuestions = await questions.find({})

            res.json({
                status: 'Success get all questions!',
                data: allQuestions
            })
        } catch(error) {
            console.error(error);
        }
    }

    async getOne(req, res) {
        try {
            const questionAndAnswer = await questions.findOne({
                _id: req.params.id
            })

            const optionFromQuestion = await options.find({
                question:  req.params.id
            }).select('option answer')

            res.json({
                status : 'success to get question and options',
                question: questionAndAnswer,
                option: optionFromQuestion
            })

        } catch(error) {
            console.error(error);
        }
    }

    async getByTest(req, res) {
        questions.find({
            test: req.params.id
        })
        .select('question remark')
        .sort({'question': 1})
        
        
        .then(result => {
            res.json({
                message: 'Success search questions by tests',
                data: result
            })
        })
    }

    async create(req, res) {
        try {
            const getCourseId = await tests.findOne({
                _id: req.body.test_id
            })

            const createQuestions = await questions.create({
                course: getCourseId.course,
                user: req.user._id,
                test: req.body.test_id,
                question: req.body.question,
                remark: req.body.remark
            })
    
            res.json({
                status: 'Success create question!',
                data: createQuestions
            })
        } catch(error) {
            console.error(error)
        }
    }

    async update(req, res) {
        try{
            let updateObj = {}

            const getUpdate = await questions.findOne({
                _id: req.params.id
            })

            if(req.body.question) updateObj.question = req.body.question
            if(req.body.remark) updateObj.remark = req.body.remark

            const updateQuestion = await questions.findOneAndUpdate({
                _id: req.params.id
            }, { $set: updateObj }, { new: true })

            res.json({
                status: 'Success updating the question!',
                data: updateQuestion
            })
        } catch(error) {
            console.error(error);
        }
    }

    async delete(req, res) {
        try {
            const getID = await questions.findOne({
                _id: req.params.id
            })

            const deleteQuestion = await questions.delete({
                _id: req.params.id
            })

            res.json({
                status: 'Success delete question!',
                data: null
            })
        } catch(error) {
            console.error(error)
        }
    }
}

module.exports = new QuestionController;