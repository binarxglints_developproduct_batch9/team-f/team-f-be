const { studentTests } = require('../models');
const { ObjectId } = require('mongodb');

class StudentTestsController {

    async getAll(req, res) {

        try {
            const allTests = await studentTests
            .find({user: req.user._id})
            .populate('test', '_id')
            
            res.json({
                status: 'Success get all student tests!',
                data: allTests
            })

        } catch (error) {
            console.error(error);
        }
        
    }

}

module.exports = new StudentTestsController;