// IMPORT DEPENDENCIES
const { enrolls, users, lessons, studentLessons, courses, studentTests, questions, tests} = require('../models');
const { ObjectId } = require('mongodb');

// ALL METHODS FOR ENROLL
class EnrollsController {

    // METHOD FOR GET ALL DATA
    async getAll(req, res) {

        try {
            const allEnrollment = await enrolls
            .find({})
            .populate({
                path: 'course',
                populate: { path: 'user' }
              })
            
            res.json({
                status: 'Success get all enrollment!',
                data: allEnrollment
            })

        } catch (error) {
            console.error(error);
        }
        
    }

    // METHOD TO GET ONE ENROLL DETAIL
    async getOne(req, res) {

        try {
            const getEnrollment = await enrolls.find({
                _id: req.params.id
            })
            .populate({
                path: 'course',
                populate: { path: 'user' }
              })

            res.json({
                status: 'Success get enrollment!',
                data: getEnrollment
            })

        } catch (error) {
            console.error(error);
        }

    }

    // METHOD TO GET ENROLL BY COURSE
    async getByCourse(req, res) {

        try {
            const getEnrollmentByCourse = await enrolls.find({
                course: req.params.id
            })

            res.json({
                status: 'Success get enrollment per course!',
                data: getEnrollmentByCourse
            })

        } catch (error) {
            console.error(error);
        }

    }

    // METHOD TO GET ENROLL BY STUDENT
    async getByStudent(req, res) {

        try {
            const getEnrollmentByStudent = await enrolls.find({
                'user._id': new ObjectId(req.user._id),
            })
            .populate('course')
            .populate('teacher', 'name')

            res.json({
                status: 'Success get enrollment per student!',
                data: getEnrollmentByStudent
            })

        } catch (error) {
            console.error(error);
        }

    }

    async searchByName(req, res) {
        enrolls.find({
            "user.name": {
                $regex: req.query.key,
                $options: 'i'
            }
        })
        .then(result => {
            res.json({
                message: 'Success searching student by name',
                data: result
            })
        })
    }

    // METHOD TO GET ENROLL BY STUDENT
    async getByTeacher(req, res) {

        try {
            const getEnrollmentByStudent = await enrolls.find({
                teacher: req.user._id,
                course: req.params.id
            })
            
            res.json({
                status: 'Success get enrollment per teacher!',
                data: getEnrollmentByStudent
            })

        } catch (error) {
            console.error(error);
        }

    }

    // METHOD TO STUDENT ENROLL
    async createByStudent(req, res) {

        try {
            const teacherId = await courses.findOne({
                _id: req.body.course_id, 
            })
            
            const embedStudent = await users.findOne({
                _id: req.user._id
            }).select('name email')

            // CREATE NEW ENROLL TO COURSES
            const createNewEnrollment = await enrolls.create({
                user: embedStudent,
                course: req.body.course_id,
                teacher: teacherId.user
            })
            
            res.json({
                status: 'Success enroll to course!',
                data: createNewEnrollment
            })

        } catch (error) {
            console.error(error)
        }
        
    }

    // METHOD FOR TEACHER TO INVITE A STUDENT
    async inviteByTeacher(req, res) {

        try {
            // GET USER ID FROM EMAIL INPUT
            const getUserId = await users.findOne({
                email: req.body.user_email
            }).select('name email')

            // GET TEACHER DATA FROM COURSE ID
            const teacherId = await courses.findOne({
                _id: req.params.id
            })

            const createNewEnrollment = await enrolls.create({
                course: req.params.id,
                user: getUserId,
                teacher: teacherId.user,
                status: 'active'
            })

            // GET ARRAY OF LESSON BASE ON LESSON COURSES
            const getLesson = await lessons.aggregate(
                [
                    { $match: {course: new ObjectId(req.params.id)}},
                    { $sort: {'title': 1}},
                    { $project: {
                        "_id": 0,
                        "lesson": "$_id"}
                    }
                ]
            );

            // CREATE NEW ARRAY TO BE INSERTED IN STUDENT LESSON
            let listStudentLessons = []

            // PUSH LESSON TO STUDENT LESSON
            listStudentLessons = getLesson
            listStudentLessons.forEach( e => {
                e.user = getUserId._id
            })
            listStudentLessons.forEach(e => {
                e.enroll = createNewEnrollment._id
            })
            listStudentLessons.forEach(e => {
                e.course = req.params.id
            })
            listStudentLessons[0].isPermission = true

            // CREATE STUDENT LESSON BASE ON COURSE THAT HE ENROLLS
            const createEnrollLesson = await studentLessons.insertMany(
                listStudentLessons
            )
            
            // GET COURSE ID FROM LESSON
            const getEnrollLessonByEnrollment = await studentLessons
            .find({ 
                course: new ObjectId(req.params.id),
                user: new ObjectId(getUserId._id)
             })

            const updateTotalLesson = await enrolls.findOneAndUpdate({
                _id : createNewEnrollment._id
            }, { 
                totalLesson: getEnrollLessonByEnrollment.length,
            }, {new: true})

            // UPDATE COURSE TOTAL ENROLL
            const getTotalEnrolledCourse = await enrolls.find({
                course: createNewEnrollment.course
            })

            const updateCourseData = await courses.findOneAndUpdate({
                _id: createNewEnrollment.course
            }, {
                enrolled: getTotalEnrolledCourse.length
            })

            // CREATE NEW STUDENT TEST
            const getIdTests = await tests.findOne({
                course: createNewEnrollment.course
            })

            const getQuestionTotal = await questions.find({
                course: createNewEnrollment.course
            })

            const createStudentTest = await studentTests.create({
                enroll: createNewEnrollment._id,
                user: getUserId,
                test: getIdTests._id,
                totalQuestion: getQuestionTotal.length,
            })

            res.json({
                status: 'Success invite student to course!',
                data: updateTotalLesson
            })


        } catch (error) {
            console.error(error)
        }
        
    }

    async acceptByTeacher(req, res) {
        try {
            const updateEnrollment = await enrolls.findOneAndUpdate({
                _id: ObjectId(req.params.id)
            }, { status: 'active'}, { new: true })

            // GET ARRAY OF LESSON BASE ON LESSON COURSES
            const getLesson = await lessons.aggregate(
            [
                { $match: {course: updateEnrollment.course}},
                { $sort: {'title': 1}},
                { $project: {
                    "_id": 0,
                    "lesson": "$_id"}
                }
            ]
            );

            // CREATE NEW ARRAY TO BE INSERTED IN STUDENT LESSON
            let listStudentLessons = []

            // PUSH LESSON TO STUDENT LESSON
            listStudentLessons = getLesson
            listStudentLessons.forEach( e => {e.user = updateEnrollment.user._id})
            listStudentLessons.forEach(e => {e.enroll = updateEnrollment._id})
            listStudentLessons.forEach(e => {e.course = updateEnrollment.course})
            listStudentLessons[0].isPermission = true

            // CREATE STUDENT LESSON BASE ON COURSE THAT HE ENROLLS
            const createEnrollLesson = await studentLessons.insertMany(listStudentLessons)
            
            // GET COURSE ID FROM LESSON
            const getEnrollLessonByEnrollment = await studentLessons.find({ 
                course: updateEnrollment.course,
                user: updateEnrollment.user
            })

            // UPDATE TOTAL LESSON IN ENROLL
            const updateTotalLesson = await enrolls.findOneAndUpdate({
                _id : updateEnrollment._id
            }, { 
                totalLesson: getEnrollLessonByEnrollment.length,
            }, {
                new: true
            })

            // UPDATE COURSE TOTAL ENROLL
            const getTotalEnrolledCourse = await enrolls.find({
                course: updateEnrollment.course
            })

            const updateCourseData = await courses.findOneAndUpdate({
                _id: updateEnrollment.course
            }, {
                enrolled: getTotalEnrolledCourse.length
            })

             // CREATE NEW STUDENT TEST
             const getIdTests = await tests.findOne({
                course: updateEnrollment.course
            })

            const getQuestionTotal = await questions.find({
                course: updateEnrollment.course
            })

            const createStudentTest = await studentTests.create({
                enroll: updateEnrollment._id,
                user: updateEnrollment.user._id,
                test: getIdTests._id,
                totalQuestion: getQuestionTotal.length,
            })

            res.json({
                'status': 'Success updating the enrollment!',
                'data': updateTotalLesson
            })
        } catch (error) {
            console.error(error);
        }
    }

    async delete(req, res) {

        try {
            // GET EXISTING DATA
            const getData = await enrolls.findOne({
                _id: req.params.id
            })

            // DELETE ENROLL
            const deleteEnrollment = await enrolls.delete({
                _id: req.params.id
            })

            // DELETE STUDENT LESSON
            const deleteStudentLesson = await studentLessons.deleteMany({
                enroll: req.params.id
            })

            // UPDATE COURSE TOTAL ENROLLED
            const getTotalEnrolledCourse = await enrolls.find({
                course: getData.course
            })

            const updateCourseData = await courses.findOneAndUpdate({
                _id: getData.course
            }, {
                enrolled: getTotalEnrolledCourse.length
            }, {new: true})
            
            // RESULT MESSAGE
            res.json({
                status: 'Success delete enrollment!',
                message: 'all student lesson on this course deleted',
                enrolled : updateCourseData.enrolled,
                data: null
            })

        } catch (error) {
            console.error(error);
        }

    }

}

module.exports = new EnrollsController;