const { enrolls, users, lessons, options} = require('../models');
const { ObjectId } = require('mongodb');

class OptionsController {

    async getAll(req, res) {

        try {
            const allOptions = await options
            .find({})
            
            res.json({
                status: 'Success get all Option!',
                data: allOptions
            })

        } catch (error) {
            console.error(error);
        }
        
    }

    
    async getByQuestion(req, res) {

        try {
            const getOptionsbyQuestion = await options.find({
                question: req.params.id
            })

            res.json({
                status: 'Success get option per question!',
                data: getOptionsbyQuestion
            })

        } catch (error) {
            console.error(error);
        }

    }

    

    async create(req, res) {

        try {

            const createNewOption = await options.create({
                
                user: req.user._id,
                question: req.body.question_id,
                option: req.body.option,
                answer: req.body.answer === undefined ? 'false' : req.body.answer
            })


            res.json({
                status: 'Success get create lessons!',
                data: createNewOption,
            })


        } catch (error) {
            console.error(error)
        }
        
    }
    async update(req, res) {
        try {
            let updateObj = {}
            //SELECTIVE UPDATE
            if(req.body.option) updateObj.option = req.body.option
            if(req.body.answer) updateObj.answer = req.body.answer

            const updateOption = await options.findOneAndUpdate({
                _id: req.params.id
            }, { $set: updateObj }, { new: true })

            res.json({
                'status': 'Success updating the option',
                'data': updateOption
            })
        } catch (error) {
            console.error(error);
        }
    }

    
    async delete(req, res) {

        try {
            const deleteOptions = await options.delete({
                _id: req.params.id
            })
            
            res.json({
                status: 'Success delete options!',
                data: null
            })

        } catch (error) {
            console.error(error);
        }

    }

}

module.exports = new OptionsController;