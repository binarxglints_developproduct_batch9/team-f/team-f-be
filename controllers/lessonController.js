const { lessons, courses } = require('../models');
const { ObjectId } = require('mongodb');
const fs = require('fs');
const { count } = require('../models/users');

class LessonsController {

    async getAll(req, res) {

        try {
            const allCourseLessons = await lessons
            .find({})
            .sort('title')
            
            res.json({
                status: 'Success get all course lessons!',
                data: allCourseLessons
            })

        } catch (error) {
            console.error(error);
        }
        
    }

    async getOne(req, res) {

        try {
            const getCourseLessons = await lessons.find({
                _id: req.params.id
            })
            .populate('user', 'name')
            .populate('course', 'title')
            
            res.json({
                status: 'Success get course lessons!',
                data: getCourseLessons
            })

        } catch (error) {
            console.error(error);
        }

    }

    async getByCourse(req, res) {

        try {
            const getCourseLessons = await lessons.find({
                course: req.params.id
            })
            .sort('title')
            
            res.json({
                status: 'Success get course lessons!',
                data: getCourseLessons
            })

        } catch (error) {
            console.error(error);
        }

    }

    async create(req, res) {

        try {
            let obj = []
            for(let key of req.files.material) {obj.push(key.filename)}

            const createNewLessons = await lessons.create({
                
                user: req.user._id,
                title: req.body.title,
                course: req.body.course_id,
                description: req.body.description,
                video: req.files === undefined ? '' : req.files.video[0].filename,
                material: req.files === undefined ? '' : obj, 
                
            })

            // TO GET COURSE CURRENT DATA
            const getLessonNewCount = await lessons.find({
                course: req.body.course_id
            })

            // GET ARRAY OF MATERIAL DATA FROM COURSES
            let countMaterial = []
            for(let key of getLessonNewCount) {
                countMaterial.push(key.material)
            }
            // MAKE A COUNT OF EACH ARRAY MATERIAL
            let allLengths = countMaterial.map(element => {
                return element.length;
            });

            // REDUCER METHODS TO COUNT ALL MATERIAL
            const reducer = (accumulator, currentValue) => accumulator + currentValue;


            // UPDATE COURSE LESSON AND MATERIAL PROPERTY 
            const updateCourseCounts = await courses.findOneAndUpdate({
                _id: req.body.course_id
            },
            {
                lesson: getLessonNewCount.length,
                material: allLengths.reduce(reducer)
            }, {new:true})

            // RESULT
            res.json({
                status: 'Success get create lessons!',
                totalLessons: updateCourseCounts.lesson,
                totalMaterial: updateCourseCounts.material,
                data: createNewLessons,
            })


        } catch (error) {
            console.error(error)
        }
        
    }

    async update(req, res) {
        try {
            const getId = await lessons.findOne({
                _id: req.params.id
            })

            // DELETE IMAGE ON FILE SYSTEM
            if(req.files.video) {
                let trim = getId.video.slice(39)
                const path = "./public/lessonAssets/" + trim
                const deleteFile = await fs.unlinkSync(path)
            }

            let obj = []
            if(req.files.material) {
                let trim = []
                for(let key of getId.material) {
                    trim.push(key.slice(39))
                }
                for(let key of trim) {
                    const deleteFile = await fs.unlinkSync("./public/lessonAssets/" + key)
                }
                for(let key of req.files.material) {
                    obj.push(key.filename)
                }
            }

            
            let updateObj = {}

            //SELECTIVE UPDATE
            if(req.files.video && req.files.video[0].filename) updateObj.video = req.files ? req.files.video[0].filename : " "
            if(req.files.material && req.files.material[0]) updateObj.material = req.files ? obj : " "
            if(req.body.title) updateObj.title = req.body.title
            if(req.body.description) updateObj.description = req.body.description

            const updateLesson = await lessons.findOneAndUpdate({
                _id: req.params.id
            }, { $set: updateObj }, { new: true })

            // TO GET COURSE CURRENT DATA
            const getLessonNewCount = await lessons.find({
                course: getId.course
            })

            // GET ARRAY OF MATERIAL DATA FROM COURSES
            let countMaterial = []
            for(let key of getLessonNewCount) {
                countMaterial.push(key.material)
            }
            // MAKE A COUNT OF EACH ARRAY MATERIAL
            let allLengths = countMaterial.map(element => {
                return element.length;
            });

            // REDUCER METHODS TO COUNT ALL MATERIAL
            const reducer = (accumulator, currentValue) => accumulator + currentValue;


            // UPDATE COURSE LESSON AND MATERIAL PROPERTY 
            const updateCourseCounts = await courses.findOneAndUpdate({
                _id: getId.course
            },
            {
                lesson: getLessonNewCount.length,
                material: allLengths.reduce(reducer)
            }, {new:true})

            // RESULT
            res.json({
                status: 'Success updating the lesson!',
                totalLessons: updateCourseCounts.lesson,
                totalMaterial: updateCourseCounts.material,
                data: updateLesson
            })
        } catch (error) {
            console.error(error);
        }
    }

    async delete(req, res) {

        try {
            const getCourseId = await lessons.findOne({
                _id: req.params.id
            })

            const deleteCourseLessons = await lessons.delete({
                _id: req.params.id
            })

            // TO GET COURSE CURRENT DATA
            const getLessonNewCount = await lessons.find({
                course: getCourseId.course
            })

            // GET ARRAY OF MATERIAL DATA FROM COURSES
            let countMaterial = []
            for(let key of getLessonNewCount) {
                countMaterial.push(key.material)
            }
            // MAKE A COUNT OF EACH ARRAY MATERIAL
            let allLengths = countMaterial.map(element => {
                return element.length;
            });

            // REDUCER METHODS TO COUNT ALL MATERIAL
            const reducer = (accumulator, currentValue) => accumulator + currentValue;


            // UPDATE COURSE LESSON AND MATERIAL PROPERTY 
            const updateCourseCounts = await courses.findOneAndUpdate({
                _id: getCourseId.course
            },
            {
                lesson: getLessonNewCount.length,
                material: allLengths.reduce(reducer)
            }, {new:true})
            
            // DELETE IMAGE ON FILE SYSTEM
            let trimVideo = getCourseId.video.slice(39)
            const path = "./public/lessonAssets/" + trimVideo
            const deleteFile = await fs.unlinkSync(path)
            
            let trimMaterial = []
            for(let key of getCourseId.material) {
                trimMaterial.push(key.slice(39))
            }
            for(let key of trimMaterial) {
                const deleteFile = await fs.unlinkSync("./public/lessonAssets/" + key)
            }
                
            res.json({
                status: 'Success delete course lessons!',
                totalLessons: updateCourseCounts.lesson,
                totalMaterial: updateCourseCounts.material,
                data: null
            })

        } catch (error) {
            console.error(error);
        }

    }

}

module.exports = new LessonsController;