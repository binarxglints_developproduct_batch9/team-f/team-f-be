const { studentOptions, options, questions, studentTests, enrolls } = require('../models');
const { ObjectId } = require('mongodb');

class StudentTestsController {

    async getAll(req, res) {

        try {
            const allOptions = await studentOptions
            .find({
                user: req.user._id
            })
            
            res.json({
                status: 'Success get all student tests!',
                data: allOptions
            })

        } catch (error) {
            console.error(error);
        }
        
    }

    async getbyQuestion(req, res) {

        try {
            const allOptions = await studentOptions.find({
                question: req.params.id
            })
            
            res.json({
                status: 'Success get question options!',
                data: allOptions
            })

        } catch (error) {
            console.error(error);
        }
        
    }

    async getbyTest(req, res) {

        try {
            const getStudentTest = await studentTests.findOne({
                test: req.params.id,
                user: req.user._id
            })

            if(getStudentTest){
                const allOptions = await studentOptions.find({
                    test: req.params.id,
                    studentTest: getStudentTest._id
                })
                .populate('question', 'question remark')
                .populate('answer', 'option answer')
            
                res.json({
                    status: 'Success get student answer!',
                    data: allOptions
                })
            } else {

                res.json({
                    status: 'Success get student answer!',
                    data: []
                })
            }

        } catch (error) {
            console.error(error);
        }
        
    }

    async create(req, res) {

        try {

            // GET TEST ID FROM QUESTION ID
            const getTestId = await questions.findOne({
                _id : req.body.question_id
            })
            
            // CREATE STUDENT ANSWER SUBMIT
            const allOptions = await studentOptions.create({
                question: req.body.question_id,
                answer: req.body.option_id,
                test: getTestId.test,
                studentTest: req.body.student_test_id
            })
            
            // GET STUDENT INPUT ANSWER FROM OPTIONS
            const findAnswer = await options.findOne({
                _id: allOptions.answer
            })

            // UPDATE STUDENT ANSWER SUBMIT TO RESULT FROM STUDENT CHOICE
            const result = await studentOptions.findOneAndUpdate(
                {_id: allOptions._id }, {result: findAnswer.answer}, {new: true}
            )
            
            // COUNT TRUE ANSWER FROM USER
            const countTrueAnswer = await studentOptions.find({
                studentTest: new ObjectId(req.body.student_test_id),
                result: 'true'
            })
            .populate('question', 'question remark')
            .populate('answer', 'option answer')
            .select({createdAt: -1})

            // UPDATE STUDENT TEST SCORE AND TOTAL QUESTION TRUE
            const studentTestCount = await studentTests.findOne({
                _id: new ObjectId(req.body.student_test_id)
            })
            const getTotalQuestion = countTrueAnswer.length / studentTestCount.totalQuestion
            const updateTestResult = await studentTests.findOneAndUpdate({
                _id: req.body.student_test_id
            },{
                totalCorrect: countTrueAnswer.length,
                score: getTotalQuestion * 100
            }, {new: true})

            const updateStudentEnrolls = await enrolls.findOneAndUpdate({
                _id: updateTestResult.enroll
            }, {
                score:updateTestResult.score,
                status: 'completed'
             }, {new: true})
            
            res.json({
                status: 'Success get all student tests!',
                data: result
            })

        } catch (error) {
            console.error(error);
        }
        
    }

    async delete(req, res) {

        try {
            const allOptions = await studentOptions.delete({
                _id: req.params.id
            })
            
            res.json({
                status: 'Success get student answer!',
                data: null
            })

        } catch (error) {
            console.error(error);
        }
        
    }

}

module.exports = new StudentTestsController;