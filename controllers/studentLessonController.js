const { studentLessons, enrolls } = require('../models');
const { ObjectId } = require('mongodb');

class StudentLessonsController {

    async getAll(req, res) {

        try {
            const allStudentLesson = await studentLessons
            .find({})
            .populate('lesson', 'title')
            
            res.json({
                status: 'Success get all student lesson!',
                data: allStudentLesson
            })

        } catch (error) {
            console.error(error);
        }
        
    }

    async getOne(req, res) {

        try {
            const getStudentLesson = await studentLessons
            .find({ 
                _id: req.params.id,
                isPermission: 'true'
            })
            .populate('lesson')
            .populate('user', 'name email')
            .populate('course')

            res.json({
                status: 'Success get student lesson!',
                data: getStudentLesson
            })

        } catch (error) {
            console.error(error);
        }

    }

    async getByEnrollment(req, res) {

        try {
            const getStudentLessonByEnroll = await studentLessons
            .aggregate([
                { $lookup: { 
                    from: "lessons", 
                    localField: "lesson", 
                    foreignField: "_id", 
                    as: "lesson" 
                }},
                { $unwind: "$lesson" },
                { $sort: {'lesson.title': 1}},
                { $match: { 
                    course: new ObjectId(req.params.id),
                    user: new ObjectId(req.user._id)
                }},
            //     { $set: {
            //         "lesson.video": { "$concat": ["https://lektur.kuyrek.com/lessonAssets/","$lesson.video"]},
            //     }},
            ])

            // if(getStudentLessonByEnroll){
            //     for (let i=0; i<getStudentLessonByEnroll.length; i++) {
            //         for (let k=0; k<getStudentLessonByEnroll[i].lesson.material.length; k++) {
            //             getStudentLessonByEnroll[i].lesson.material[k] = "https://lektur.kuyrek.com/lessonAssets/" + getStudentLessonByEnroll[i].lesson.material[k]
            //         }
            //     }
            // }
          
            res.json({
                status: 'Success get lesson per course!',
                data: getStudentLessonByEnroll
            })

        } catch (error) {
            console.error(error);
        }

    }

    async isCompleted(req, res) {
        try {
            // TO COMPLETED THE LESSON
            const updateStudentLesson = await studentLessons.findOneAndUpdate({
                _id: req.params.id}, { isCompleted: true }, { new: true })

            // TO COUNT THE PROGRESS LESSONS
            const countStudentLessonFinished = await studentLessons.find({ 
                user: req.user._id,
                enroll: updateStudentLesson.enroll,
                isCompleted: true
            })

            // TO UPDATE ENROLLED COURSE PROFRESS
            const getCurrentCompleted = await enrolls
            .findOne({ 
                _id: updateStudentLesson.enroll
            })

            const getAllCompleted = await studentLessons.find({
                enroll : updateStudentLesson.enroll,
                isCompleted: true
            })
            
            const updateTotalCompletedInEnroll = await enrolls.findOneAndUpdate({
                _id: updateStudentLesson.enroll }, 
                { totalLessonCompleted: countStudentLessonFinished.length }, {new:true}
            )
            
            // UNLOCK NEXT LESSON
            const lessonData = await studentLessons
            .aggregate([
                { $lookup: { 
                    from: "lessons", 
                    localField: "lesson", 
                    foreignField: "_id", 
                    as: "lesson" 
                }},
                { $unwind: "$lesson" },
                { $sort: {'lesson.title': 1}},
                { $match: { 
                    enroll: new ObjectId(updateStudentLesson.enroll),
                    isPermission: false
                }}
            ])
            // const lessonData = await studentLessons.find({
            //     enroll : updateStudentLesson.enroll,
            //     isPermission: false
            // })

            if(lessonData.length !== 0){
                // UPDATE THE PERMISSION
                const updatePermission = await studentLessons.findOneAndUpdate({
                    _id : lessonData[0]._id
                }, {isPermission: true}, {new:true})
            }

            // RESULT
            res.json({
                'status': 'Success completed the lesson',
                'data': updateStudentLesson,
                'progress': `${updateTotalCompletedInEnroll.totalLessonCompleted} / ${updateTotalCompletedInEnroll.totalLesson}`
            })
            
        } catch (error) {
            console.error(error);
        }
    }

    async delete(req, res) {

        try {
            const deleteStudnetLesson = await studentLessons
            .delete({
                _id: req.params.id
            })
            
            res.json({
                status: 'Success delete student lesson!',
                data: null
            })

        } catch (error) {
            console.error(error);
        }

    }

}

module.exports = new StudentLessonsController;