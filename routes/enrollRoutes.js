const express = require('express');
const router = express.Router();
const passport = require('passport')
const auth = require('../middlewares/auth')
const EnrollController = require('../controllers/enrollController');
const enrollValidator = require('../middlewares/validators/enrollValidator');

// GET ALL ENROLLMENT
router.get('/', 
    EnrollController.getAll);

// GET ONE ENROLLMENT
router.get('/:id', 
    enrollValidator.getOne, 
    EnrollController.getOne);

// GET ENROLLMENT BY COURSE
router.get('/get/byCourse/:id', 
    enrollValidator.getByCourse, 
    EnrollController.getByCourse);

// GET ENROLLMENT BY STUDENT
router.get('/get/byStudent', [
    passport.authenticate('student',
    { session: false }),
    enrollValidator.getByStudent
],  EnrollController.getByStudent);

// GET ENROLLMENT BY STUDENT
router.get('/get/byTeacherCourse/:id', [
    passport.authenticate('teacher',
    { session: false }),
    enrollValidator.getByTeacher
],  EnrollController.getByTeacher);

// GET ENROLLMENT BY STUDENT
router.get('/search/byName', [
    passport.authenticate('teacher',
    { session: false }),
],  EnrollController.searchByName);

// CREATE ENROLLMENT BY STUDENT
router.post('/create/byStudent', [
    passport.authenticate('student',
    { session: false }),
    enrollValidator.createByStudent
], EnrollController.createByStudent);

// INVITE ENROLLMENT BY TEACHER
router.post('/invite/course/:id', [
    passport.authenticate('teacher',
    { session: false }),
    enrollValidator.inviteByTeacher
], EnrollController.inviteByTeacher);

// ACCEPT BY TEACHER
router.patch('/accept/:id', [
    passport.authenticate('teacher',
    { session: false }),
    enrollValidator.acceptByTeacher
], EnrollController.acceptByTeacher)

//DELETE COURSE
router.delete('/delete/:id/', [
    passport.authenticate('teacher',
    { session: false }),
    enrollValidator.delete
], EnrollController.delete);

module.exports = router;