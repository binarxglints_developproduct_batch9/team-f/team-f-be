const express = require('express');
const router = express.Router();
const passport = require('passport')
const auth = require('../middlewares/auth')
const StudentLessonController = require('../controllers/studentLessonController');
const studentLessonValidator = require('../middlewares/validators/studentLessonValidator');

// GET ALL ENROLLMENT
router.get('/', 
    StudentLessonController.getAll);

// GET ONE ENROLLMENT
router.get('/:id', 
    studentLessonValidator.getOne, 
    StudentLessonController.getOne);

// GET ENROLLMENT BY COURSE
router.get('/get/byEnrollment/:id',[
    passport.authenticate('student',
    { session: false }),
    studentLessonValidator.getByEnrollment
],  StudentLessonController.getByEnrollment);


// INVITE ENROLLMENT BY TEACHER
router.patch('/completed/:id', [
    passport.authenticate('student',
    { session: false }),
    studentLessonValidator.isCompleted
], StudentLessonController.isCompleted);


//DELETE COURSE
router.delete('/delete/:id/', [
    passport.authenticate('student',
    { session: false }),
    studentLessonValidator.delete
], StudentLessonController.delete);

module.exports = router;