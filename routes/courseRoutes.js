const express = require('express');
const router = express.Router();
const passport = require('passport')
const auth = require('../middlewares/auth')
const CourseController = require('../controllers/courseController');
const courseValidator = require('../middlewares/validators/courseValidator');

// GET ALL COURSES
router.get('/all', CourseController.getAll);

// GET ONE COURSE
router.get('/:id', courseValidator.getOne, CourseController.getOne);

// GET COURSES BY TEACHER
router.get('/find/byTeacher', [
    passport.authenticate('profile', 
    { session: false })
], CourseController.searchByTeacher);

//GET COURSES BY CATEGORY
router.get('/find/byCategory',  
    CourseController.searchByCategory);

//GET COURSES BY CATEGORY
router.get('/find/byTitle',  
    CourseController.searchByTitle);

// CREATE COURSES
router.post('/create', [
    passport.authenticate('teacher',
    { session: false }),
    courseValidator.create
], CourseController.create);

// PUBLISH COURSES
router.put('/publish/:id', [
    passport.authenticate('teacher',
    { session: false }),
    courseValidator.publish
], CourseController.publishCourse);

//UPDATE COURSES DETAIL
router.get('/unpublished/course', [
    passport.authenticate('teacher',
    { session: false }),
], CourseController.getUnpublished)

//UPDATE COURSES DETAIL
router.patch('/update/:id', [
    passport.authenticate('teacher',
    { session: false }),
    courseValidator.update
], CourseController.update)

//DELETE COURSE
router.delete('/delete/:id/', [
    passport.authenticate('teacher',
    { session: false }),
    courseValidator.delete
], CourseController.delete);

module.exports = router;