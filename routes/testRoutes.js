const express = require('express');
const router = express.Router();
const passport = require('passport');
const auth = require('../middlewares/auth');
const testValidator = require('../middlewares/validators/testValidator');
const TestController = require('../controllers/testController');

//GET ALL TESTS
router.get('/all', [
    passport.authenticate('teacher', 
    { session: false })
], TestController.getAll);

//GET ONE TEST
router.get('/:id', [
    passport.authenticate('profile',
    { session: false }),
    testValidator.getOne
], TestController.getOne);

//GET TEST BY COURSE
router.get('/ByCourse/:id', [
    passport.authenticate('profile', 
    { session: false }),
], TestController.getByCourse);

//CREATE TEST
router.post('/create', [
    passport.authenticate('teacher',
    { session: false }),
    testValidator.create
], TestController.create);

//DELETE TEST
router.delete('/delete/:id', [
    passport.authenticate('teacher', 
    { session: false }),
    testValidator.delete
], TestController.delete)

module.exports = router;