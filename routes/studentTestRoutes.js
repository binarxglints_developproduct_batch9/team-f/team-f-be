const express = require('express');
const router = express.Router();
const passport = require('passport')
const auth = require('../middlewares/auth')
const StudentTestController = require('../controllers/studentTestController');
const StudentOptionController = require('../controllers/studentOptionController');
const StudentTestValidator = require('../middlewares/validators/studentTestValidator');

// GET ALL ENROLLMENT
router.get('/', 
passport.authenticate('student',
    { session: false }),
StudentTestController.getAll);

router.get('/all/answer', 
passport.authenticate('student',
    { session: false }),
StudentOptionController.getAll);

router.get('/options/question/:id', 
StudentOptionController.getbyQuestion);

router.get('/student/test/:id', [
    passport.authenticate('student',
    { session: false }),
    ], StudentOptionController.getbyTest);

router.post('/create/answer',[
    passport.authenticate('student',
    { session: false }),
    StudentTestValidator.create
], StudentOptionController.create);

router.delete('/delete/answer/:id',[
    passport.authenticate('admin',
    { session: false }),
    StudentTestValidator.delete
], StudentOptionController.delete);

module.exports = router;