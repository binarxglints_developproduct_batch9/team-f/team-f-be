const express = require('express');
const router = express.Router();
const passport = require('passport')
const auth = require('../middlewares/auth')
const LessonsController = require('../controllers/lessonController');
const lessonsValidator = require('../middlewares/validators/lessonValidator');

// ENDPOINT TO GET ALL COURSE LESSONS
router.get('/', LessonsController.getAll)

// ENDPOINT TO GET ONE COURSE LESSONS
router.get('/:id', 
    lessonsValidator.getOne ,
    LessonsController.getOne)

// ENDPOINT TO GET ONE COURSE LESSONS
router.get('/byCourse/:id', 
    lessonsValidator.getByCourse ,
    LessonsController.getByCourse)

// ENDPOINT TO CREATE NEW COURSE LESSON
router.post('/create', [
    passport.authenticate('teacher',
    { session: false }),
    lessonsValidator.create
], LessonsController.create)

// ENDPOINT TO UPDATE COURSE LESSON
router.patch('/update/:id', [
    passport.authenticate('teacher',
    { session: false }),
    lessonsValidator.update
], LessonsController.update)

// ENDPOINT TO DELETE COURSE LESSON
router.delete('/delete/:id', [
    passport.authenticate('teacher',
    { session: false }),
    lessonsValidator.delete
], LessonsController.delete)


module.exports = router;