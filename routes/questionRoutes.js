const express = require('express');
const router = express.Router();
const passport = require('passport');
const auth = require('../middlewares/auth');
const questionValidator = require('../middlewares/validators/questionValidator');
const QuestionController = require('../controllers/questionController');

//GET ALL QUESTIONS
router.get('/all', QuestionController.getAll);

//GET ONE QUESTION
router.get('/one/:id', [
    passport.authenticate('profile', 
    { session: false }),
    questionValidator.getOne
], QuestionController.getOne);

//GET ASSESMENT PER COURSE
router.get('/byTest/:id', [
    passport.authenticate('profile',
    { session: false }),
    questionValidator.getByTest
], QuestionController.getByTest);

//CREATE QUESTIONS (ONLY ACCESSIBLE BY TEACHER)
router.post('/create', [
    passport.authenticate('teacher', 
    { session: false }),
    questionValidator.create
], QuestionController.create);

//UPDATE QUESTIONS (ONLY ACCESSIBLE BY TEACHER)
router.patch('/update/:id', [
    passport.authenticate('teacher', 
    { session: false }),
    questionValidator.update
], QuestionController.update);

//DELETE QUESTIONS (ONLY ACCESSIBLE BY TEACHER)
router.delete('/delete/:id', [
    passport.authenticate('teacher',
    { session: false }),
    questionValidator.delete
], QuestionController.delete);

module.exports = router;