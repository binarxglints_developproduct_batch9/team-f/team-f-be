const express = require('express');
const router = express.Router();
const passport = require('passport')
const auth = require('../middlewares/auth')
const OptionsController = require('../controllers/optionController');
const optionsValidator = require('../middlewares/validators/optionValidator');

// ENDPOINT TO GET ALL COURSE LESSONS
router.get('/', OptionsController.getAll)


//GET OPTION  BY QUESTION
router.get('/find/byQuestion/:id', [
    passport.authenticate('profile', 
    { session: false })
], OptionsController.getByQuestion);


// ENDPOINT TO CREATE NEW COURSE LESSON
router.post('/create', [
    passport.authenticate('teacher',
    { session: false }),
    optionsValidator.create
], OptionsController.create)


router.patch('/update/:id', [
    passport.authenticate('teacher',
    { session: false }),
    optionsValidator.update
], OptionsController.update)

// ENDPOINT TO DELETE COURSE LESSON
router.delete('/delete/:id', [
    passport.authenticate('teacher',
    { session: false }),
    optionsValidator.delete
], OptionsController.delete)


 module.exports = router;