// IMPORT DEPENDENCIES
const express = require('express')
const router = express.Router() 
const passport = require('passport')
const auth = require('../middlewares/auth')
const UserController = require('../controllers/userController')
const userValidator = require('../middlewares/validators/userValidator')
const userController = require('../controllers/userController')

  // ENDPOINT FOR SIGNUP
  router.post('/signup', [
    userValidator.signup, 
    passport.authenticate('signup', 
    { session: false })
  ], UserController.signup);

  // ENDPOINT FOR SIGNUP
  router.get('/verification/:token', [
  ], UserController.verification);

  // ENDPOINT FOR SIGNUP TEACHER
  router.post('/signupTeacher', [
    userValidator.signup, 
    passport.authenticate('signupTeacher', 
    { session: false })
  ], UserController.signup);

  router.get('/auth/google',
    passport.authenticate('google', 
    { scope: ['profile', 'email'] })
  );

  router.get('/auth/google/callback', 
  passport.authenticate('google', 
  { session: false, failureRedirect: '/failed' }),
  userController.login
  );

  router.get('/failed', UserController.failed)

  // ENDPOINT FOR LOGIN
  router.post('/login', [
    userValidator.login, 
    passport.authenticate('login', 
    { session: false })
  ], UserController.login);

  // ENDPOINT FOR GET ALL USER PROFILE
  router.get('/userProfile/all',
  UserController.getAll);

  // ENDPOINT FOR GET USER PROFILE
  router.get('/userProfile', 
    passport.authenticate('profile', 
    { session: false }), 
    UserController.getUserProfile);

  // ENDPOINT FOR UPDATE USER PROFILE
  router.patch('/userProfile/update', [ 
    userValidator.update, 
    passport.authenticate('profile', 
    { session: false })
  ], UserController.updateUserProfile);

  // ENDPOINT FOR DELETE USER PROFILE
  router.delete('/delete/:id/', [ 
    passport.authenticate('admin', 
    { session: false }),
    userValidator.delete 
  ], UserController.delete);

module.exports = router; // export router
