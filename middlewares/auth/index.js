// AUTHENTICATION DEPENDENCIES
const path = require('path')
require('dotenv').config({ path: `.env.${process.env.NODE_ENV}`})
const passport = require('passport')
const localStrategy = require('passport-local').Strategy
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const { users } = require('../../models')
const bcrypt = require('bcrypt')
const JWTstrategy = require('passport-jwt').Strategy
const ExtractJWT = require('passport-jwt').ExtractJwt
const key = 'lektur_secret_key'

// STRATEGY FOR SIGNUP
passport.use(
  'signup',
  new localStrategy({
      'usernameField': 'email',
      'passwordField': 'password',
      passReqToCallback: true
    },

    // CONTROLLER FOR CREATE NEW USER
    async (req, email, password, done) => {
      users.create({
        name: req.body.name, 
        email: email, 
        password: password, 
      })

      // IF SUCCESS, IT WILL RETURN AUTHORIZATION WITH REQ.USER
      .then(user => {
        return done(null, user, {
          message: 'User created!'
        })
      })
      
      // IF ERROR, IT WILL RETURN UNAUTHORIZED
      .catch(err => {
        return done(null, false, {
          message: "User can't be created"
        })
      })
    },
  )
);

passport.use(
  'signupTeacher',
  new localStrategy({
      'usernameField': 'email',
      'passwordField': 'password',
      passReqToCallback: true
    },

    // CONTROLLER FOR CREATE NEW USER
    async (req, email, password, done) => {
      users.create({
        name: req.body.name, 
        email: email, 
        password: password,
        role: "teacher" 
      })

      // IF SUCCESS, IT WILL RETURN AUTHORIZATION WITH REQ.USER
      .then(user => {
        return done(null, user, {
          message: 'User created!'
        })
      })
      
      // IF ERROR, IT WILL RETURN UNAUTHORIZED
      .catch(err => {
        return done(null, false, {
          message: "User can't be created"
        })
      })
    },
  )
);

// STRATEGY FOR LOGIN
passport.use(
    'login',
    new localStrategy({
        'usernameField': 'email', // username from req.body.email
        'passwordField': 'password' // password from req.body.password
      },

      // CONTROLLER FOR USER TO LOGIN
      async (email, password, done) => {
        
        // CHECK EMAIL IN DATABASE
        const chekUserEmail = await users.findOne({
            email: email
        })
  
        // IF USER NOT FOUND
        if (!chekUserEmail) {
          return done(null, false, {
            message: 'User is not found!'
          })
        }
  
        // IF USER FOUND, CONTINUE TO VALIDATE THE HASH PASSWORD WITH BCRYPT
        const validatePassword = await bcrypt.compare(password, chekUserEmail.password);
  
        // IF THE PASSWORD IS WRONG
        if (!validatePassword) {
          return done(null, false, {
            message: 'Wrong password!'
          })
        }
  
        // IF TRUE
        return done(null, chekUserEmail, {
          message: 'Login success!'
        })
      }
    )
)

// AUTHENTICATION GOOGLE FOR STUDENT
passport.use(
  'google',
  new GoogleStrategy({
  clientID: process.env.GOOGLE_CLIENT_ID,
  clientSecret: process.env.GOOGLE_CLIENT_SECRET,
  callbackURL: process.env.GOOGLE_CALLBACK_URL
},
async (accessToken, refreshToken, profile, done) => {

  try {
    let user = await users.findOne({
      email: profile.emails[0].value
    })
    
    if(!user) {
      user = await users.create({
        name: `${profile.name.givenName}`,
        email: profile.emails[0].value,
        password: "defaultpassword",
        role: 'student'
      })
    }

    return done(null, user)
  } catch (e) {
    return done(null, false)
  }
   
}
));

// AUTHENTICATION FOR STUDENT
passport.use(
    'student',
    new JWTstrategy({
        secretOrKey: key,
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken()
      },

      // CHECK IF THERE IS USER IN DATABASE
      async (token, done) => {
        const userLogin = await users.findOne({
          _id: token.user._id
        })

        // IF USER ROLE === STUDENT
        if (userLogin.role.includes('student')) {
          return done(null, token.user)
        }

        // IF USER ROLE !== STUDENT
        return done(null, false)
      }
    )
)

// AUTHENTICATION FOR TEACHER
passport.use(
    'teacher',
    new JWTstrategy({
        secretOrKey: key,
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken()
      },

      // CHECK IF THERE IS USER IN DATABASE
      async (token, done) => {
        const userLogin = await users.findOne({
          _id: token.user._id
        })

        // IF USER ROLE === STUDENT
        if (userLogin.role.includes('teacher')) {
          return done(null, token.user)
        }

        // IF USER ROLE !== STUDENT
        return done(null, false)
      }
    )
)

// AUTHENTICATION FOR GET PROFILE INFO
passport.use(
    'profile',
    new JWTstrategy({
        secretOrKey: key,
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken()
      },

      // CHECK IF THERE IS USER IN DATABASE
      async (token, done) => {
        const userLogin = await users.findOne({
          _id: token.user._id
        })

        // IF USER ROLE === STUDENT
        if (userLogin.role.includes('student') || userLogin.role.includes('teacher')) {
          return done(null, token.user)
        }

        // IF USER ROLE !== STUDENT
        return done(null, false)
      }
    )
)

// AUTHENTICATION FOR ADMIN
passport.use(
  'admin',
  new JWTstrategy({
      secretOrKey: key,
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken()
    },

    // CHECK IF THERE IS USER IN DATABASE
    async (token, done) => {
      const userLogin = await users.findOne({
        _id: token.user._id
      })

      // IF USER ROLE === STUDENT
      if (userLogin.role.includes('admin')) {
        return done(null, token.user)
      }

      // IF USER ROLE !== STUDENT
      return done(null, false)
    }
  )
)
  
  
