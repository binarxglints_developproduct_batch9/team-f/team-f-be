const { user, courses } = require('../../models')
const {
    check,
    validationResult, 
    matchedData,
    sanitize
} = require('express-validator')
const multer = require('multer')
const path = require('path')
const crypto = require('crypto')


const uploadDir = '/imgCourses/'
const storage = multer.diskStorage({
    destination: './public' + uploadDir,
    filename: function(req, file, cb) {
        crypto.pseudoRandomBytes(16, function(err, raw) {
            if(err) return cb(err)
            cb(null, raw.toString('hex') + path.extname(file.originalname))
        })
    }
})

const upload = multer({
    storage: storage,
    dest: uploadDir
})

module.exports = {

    create: [
        check('question_id').custom(value => {
            return questions.findOne({
                _id: value
            }).then(result => {
                if(!result) {
                    throw new Error('question not found!')
                }
            })
        }),
        check('question', 'question must be objectId!'),
        check('option', 'option must be a string').isString().notEmpty(),

        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                });
            }
            next();
        }
    ],

    getByQuestion: [
        check('id').custom(value => {
            return questions.findOne({
                _id: value
            }).then(result => {
                if(!result) {
                    throw new Error('question not found!')
                }
            })
        }),

        (req, res, next) => {
            const errors = validationResult(req)
            if(!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],


    update: [
        check('id').custom(value => {
            return options.findOne({
                _id: value
            }).then(result => {
                if(!result) {
                    throw new Error('options not found!')
                }
            })
        }),

        (req, res, next) => {
            const errors = validationResult(req);
            if(!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                });
            }
            next();
        }
    ],

    delete: [
        check('id').custom(value => {
            return options.findOne({
                _id: value
            }).then(result => {
                if(!result) {
                    throw new Error('Option not found!')
                }
            })
        }),

        (req, res, next) => {
            const errors = validationResult(req);
            if(!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                });
            }
            next();
        }
    ]
}