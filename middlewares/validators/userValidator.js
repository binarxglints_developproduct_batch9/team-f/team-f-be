// IMPORT MODULES
const { user, courses } = require('../../models')
const { check, validationResult, matchedData, sanitize } = require('express-validator')
const multer = require('multer')
const path = require('path')
const crypto = require('crypto')

// CREATED STORAGE FOLDER FOR IMAGE
const uploadDir = '/imageUser/'
const storage = multer.diskStorage({
    destination: './public' + uploadDir,
    filename: function (req, file, cb) {
        crypto.pseudoRandomBytes(16, function (err, raw) {
            if (err) return cb(err)
            cb(null, 
                raw.toString('hex') 
                + path.extname(file.originalname))
        })
    }
})
const upload = multer({ storage: storage, dest: uploadDir })

  
  module.exports = {

    // VALIDATOR INPUT FOR SIGNUP
    signup: [
      // VALIDATING EMAIL
      check('email', 'Email field must be an email address').normalizeEmail().isEmail(),

      // VALIDATING EMAIL IF THE EMAIL IS ALREADY EXIST OR NOT
      check('email', 'Email is already exist').custom(value => {
          return users.findOne({
              email: value
          }).then(result => {
              if(result) {
                  throw new Error('Email is already exist')
              }
          })
      }),

      check('name', 'name field must be string').isString().notEmpty(),
      check('email', 'email field must be email address').normalizeEmail().isEmail(),
      check('password', 'password field must have 8 to 32 characters').isString().isLength({ min: 8, max:32 }),

      // CONTINUE TO NEXT MIDDLEWARE
      (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(422).json({
            errors: errors.mapped()
          });
        }
        next();
      }
    ],

    // VALIDATOR INPUT FOR LOGIN
    login: [
      check('email', 'email field must be email address').normalizeEmail().isEmail(),
      check('password', 'password field must have 8 to 32 characters').isString().isLength({ min: 8, max:32 }),

      // CONTINUE TO NEXT MIDDLEWARE
      (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(422).json({
            errors: errors.mapped()
          });
        }
        next();
      }
    ],

    update: [
      upload.single('image'),

      // CONTINUE TO NEXT MIDDLEWARE
      (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(422).json({
            errors: errors.mapped()
          });
        }
        next();
      }
    ],

    delete: [
      check('id', 'User not found').custom(value => {
        return users.findOne({
            _id: value
        }).then(result => {
            if(!result) {
                throw new Error('User not found')
            }
        })
      }),

      // CONTINUE TO NEXT MIDDLEWARE
      (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(422).json({
            errors: errors.mapped()
          });
        }
        next();
      }
    ]

  }
  