const {  users, questions, courses, tests } = require('../../models');
const {
    check,
    validationResult,
    matchedData,
    sanitize
} = require('express-validator');

module.exports = {
    
    getOne: [
        check('id').custom(value => {
            return questions.findOne({
                _id: value
            }).then(result => {
                if (!result) {
                    throw new Error('Question Id not found!');
                }
            })
        }),

        (req, res, next) => {
            const errors = validationResult(req)
            if(!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],

    getByTest: [
        check('id').custom(value => {
            return tests.findOne({
                _id: value
            }).then(result => {
                if(!result) {
                    throw new Error('Test not found!');
                }
            })
        }),

        (req, res, next) => {
            const errors = validationResult(req)
            if(!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],

    create: [
        check('test_id').custom(value => {
            return tests.findOne({
                _id: value
            }).then(result => {
                if(!result) {
                    throw new Error('Test not found!')
                }
            })
        }),

        check('question', 'Question must be string').isString().notEmpty(),
        check('remark', 'Remark must be string').isString().notEmpty(),

        (req, res, next) => {
            const errors = validationResult(req);
            if(!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],

    update: [
        check('id').custom(value => {
            return questions.findOne({
                _id: value
            }).then(result => {
                if(!result) {
                    throw new Error('Question not found!')
                }
            })
        }),

        (req, res, next) => {
            const errors = validationResult(req)
            if(!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],

    delete: [
        check('id').custom(value => {
            return questions.findOne({
                _id: value
            }).then(result => {
                if (!result) {
                    throw new Error('Question id not found!')
                }
            })
        }),

        (req, res, next) => {
            const errors = validationResult(req)
            if(!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ]
}