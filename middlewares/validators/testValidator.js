const { tests, courses } = require('../../models');
const {
    check,
    validationResult,
    matchedData,
    sanitize 
} = require('express-validator');

module.exports = {
    
    getOne: [
        check('id').custom(value => {
            return tests.findOne({
                _id: value
            }).then(result => {
                if (!result) {
                    throw new Error('Test id not found!')
                }
            })
        }),

        (req, res, next) => {
            const errors = validationResult(req)
            if(!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],

    create: [
        check('course_id').custom(value => {
            return courses.findOne({
                _id: value
            }).then(result => {
                if(!result) {
                    throw new Error('Course not found!')
                }
            })
        }),

        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next();
        }
    ],

    delete: [
        check('id').custom(value => {
            return tests.findOne({
                _id: value
            }).then(result => {
                if(!result) {
                    throw new Error('Test not found!')
                }
            })
        }),

        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ]

}