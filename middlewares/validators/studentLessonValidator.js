// IMPORT DEPENDENCIES
const { studentLessons, courses } = require('../../models')
const {
    check,
    validationResult, 
    matchedData,
    sanitize
} = require('express-validator')

module.exports = {

    // VALIDATOR FOR GET ONE
    getOne: [
        check('id').custom(value => {
            return studentLessons.findOne({
                _id: value,
            }).then(result => {
                if(!result) {
                    throw new Error('student lesson not found!')
                }
                if(result.isPermission != true) {
                    throw new Error('Please finish previous lesson to continue!')
                }
            })
        }),
        
        (req, res, next) => {
            const errors = validationResult(req)
            if(!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }

            // MOVE TO NEXT MIDDLEWARES IF ALL MEETS THE VALIDATION
            next()
        }
    ],

    // VALIDATOR TO GET ASSESSMENT BY LESSON
    getByEnrollment: [
        check('id').custom(value => {
            return courses.findOne({
                _id: value
            }).then(result => {
                if(!result) {
                    throw new Error('enroll course not found!')
                }
            })
        }),

        (req, res, next) => {
            const errors = validationResult(req)
            if(!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }

            // MOVE TO NEXT MIDDLEWARES IF ALL MEETS THE VALIDATION
            next()
        }
    ],

    // VALIDATOR TO CREATE NEW MATERIAL
    isCompleted: [

        check('id').custom((value, {req}) => {
            return studentLessons.findOne({
                _id: value
            }).then(result => {
                if(!result) {
                    throw new Error('student lesson not found!')
                }
                if(result.isCompleted === true) {
                    throw new Error('student lesson was completed!')
                }
                if(result.user != req.user._id){
                    throw new Error('You got wrong token/ access!')
                }
            })
        }),
        
        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                });
            }

            // MOVE TO NEXT MIDDLEWARES IF ALL MEETS THE VALIDATION
            next();
        }
    ],

    // VALIDATOR TO DELETE MATERIAL
    delete: [
        check('id').custom(value => {
            return studentLessons.findOne({
                _id: value
            }).then(result => {
                if(!result) {
                    throw new Error('student lesson not found!')
                }
            })
        }),

        (req, res, next) => {
            const errors = validationResult(req);
            if(!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                });
            }
            
            // MOVE TO NEXT MIDDLEWARES IF ALL MEETS THE VALIDATION
            next();
        }
    ]
}