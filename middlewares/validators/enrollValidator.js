// IMPORT DEPENDENCIES
const {
    enrolls,
    courses,
    users
} = require('../../models')
const {
    check,
    validationResult,
    matchedData,
    sanitize
} = require('express-validator')
const {ObjectId} = require('mongodb')

module.exports = {

    // VALIDATOR FOR GET ONE
    getOne: [
        check('id').custom(value => {
            return enrolls.findOne({
                _id: value
            }).then(result => {
                if (!result) {
                    throw new Error('Enrollment not found!')
                }
            })
        }),

        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }

            // MOVE TO NEXT MIDDLEWARES IF ALL MEETS THE VALIDATION
            next()
        }
    ],

    // VALIDATOR TO GET ASSESSMENT BY LESSON
    getByCourse: [
        check('id').custom(value => {
            return courses.findOne({
                _id: value
            }).then(result => {
                if (!result) {
                    throw new Error('course not found!')
                }
            })
        }),

        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }

            // MOVE TO NEXT MIDDLEWARES IF ALL MEETS THE VALIDATION
            next()
        }
    ],

    // VALIDATOR TO GET ASSESSMENT BY LESSON
    getByStudent: [

        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }

            // MOVE TO NEXT MIDDLEWARES IF ALL MEETS THE VALIDATION
            next()
        }
    ],

    // VALIDATOR TO GET ASSESSMENT BY LESSON
    getByTeacher: [
        check('id').custom(value => {
            return courses.findOne({
                _id: value
            }).then(result => {
                if (!result) {
                    throw new Error('course not found!')
                }
            })
        }),

        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }

            // MOVE TO NEXT MIDDLEWARES IF ALL MEETS THE VALIDATION
            next()
        }
    ],

    // VALIDATOR TO CREATE NEW MATERIAL
    createByStudent: [

        check('course_id').custom(async (value, {
            req
        }) => {
            const checkCourse = await courses.findOne({
                _id: value
            })
            if (!checkCourse) throw new Error('course not found!')

            const checkStudentEnroll = await enrolls.findOne({
                "user._id": new ObjectId(req.user._id),
                course: req.body.course_id
            })

            if (checkStudentEnroll) {
                if (checkStudentEnroll.status === 'active') throw new Error('student already enroll and accepted!')
                if (checkStudentEnroll.status === 'pending') throw new Error('Student already enroll but still pending!')
                if (checkStudentEnroll.status === 'completed') throw new Error('student already completed this course')
            }
        }),

        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                });
            }

            // MOVE TO NEXT MIDDLEWARES IF ALL MEETS THE VALIDATION
            next();
        }
    ],

    // VALIDATOR TO CREATE NEW MATERIAL
    inviteByTeacher: [

        check('id').custom(value => {
            return courses.findOne({
                _id: value
            }).then(result => {
                if (!result) {
                    throw new Error('course not found!')
                }
            })
        }),

        check('user_email').custom(async (value, {
            req
        }) => {
            const checkUser = await users.findOne({
                email: value
            })

            if (!checkUser) {
                throw new Error('users not found!')
            }

            const checkStudentEnroll = await enrolls.findOne({
                "user._id": new ObjectId(checkUser._id), 
                course: req.params.id
            })

            if (checkStudentEnroll) {
                if (checkStudentEnroll.status === 'active') throw new Error('student already enroll and accepted!')
                if (checkStudentEnroll.status === 'pending') throw new Error('Student already enroll but still pending!')
                if (checkStudentEnroll.status === 'completed') throw new Error('student already completed this course')
            }

            const checkTeacher = await courses.find({
                _id: req.params.id
            })

            if (checkTeacher[0].user != req.user._id) throw new Error(`you don't have access to do this`)
        }),

        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                });
            }

            // MOVE TO NEXT MIDDLEWARES IF ALL MEETS THE VALIDATION
            next();
        }
    ],

    // VALIDATOR TO UPDATE MATERIAL
    acceptByTeacher: [

        check('id').custom(async (value, {req}) => {
            const checkEnrolls = await enrolls.findOne({
                _id: value
            })

            if (!checkEnrolls) {
                throw new Error('Enroll not found!')
            }
            
            const checkStudentEnroll = await enrolls.findOne({
                "user._id": new ObjectId(checkEnrolls.user._id), 
                course: checkEnrolls.course,
            })

            if (checkStudentEnroll) {
                if (checkStudentEnroll.status === 'active') throw new Error('student already enroll and accepted!')
                if (checkStudentEnroll.status === 'completed') throw new Error('student already completed this course')
            }

            const checkTeacher = await courses.find({
                _id: checkEnrolls.course
            })

            if (checkTeacher[0].user != req.user._id) throw new Error(`you don't have access to do this`)
        }),

        // CHECK FILE TYPE PDF AND MAX 20 MB
        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                });
            }

            // MOVE TO NEXT MIDDLEWARES IF ALL MEETS THE VALIDATION
            next();
        }
    ],

    // VALIDATOR TO DELETE MATERIAL
    delete: [
        check('id').custom(value => {
            return enrolls.findOne({
                _id: value
            }).then(result => {
                if (!result) {
                    throw new Error('Enrollment not found!')
                }
            })
        }),

        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                });
            }

            // MOVE TO NEXT MIDDLEWARES IF ALL MEETS THE VALIDATION
            next();
        }
    ]
}