const { user, courses } = require('../../models')
const {
    check,
    validationResult, 
    matchedData,
    sanitize
} = require('express-validator')
const multer = require('multer')
const path = require('path')
const crypto = require('crypto')


const uploadDir = '/imgCourses/'
const storage = multer.diskStorage({
    destination: './public' + uploadDir,
    filename: function(req, file, cb) {
        crypto.pseudoRandomBytes(16, function(err, raw) {
            if(err) return cb(err)
            cb(null, raw.toString('hex') + path.extname(file.originalname))
        })
    }
})

const upload = multer({
    storage: storage,
    dest: uploadDir
})

module.exports = {
    getOne: [
        check('id').custom(value => {
            return courses.findOne({
                _id: value
            }).then(result => {
                if(!result) {
                    throw new Error('Course not found!')
                }
            })
        }),

        (req, res, next) => {
            const errors = validationResult(req)
            if(!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],

    create: [
        upload.single('image'),

        check('title', 'Title must be strings!').isString().notEmpty(),
        check('category').isString().notEmpty(),
        check('description', 'Description must be added minimum 10 characters!').isString().isLength({
            min: 10
        }),

        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                });
            }
            next();
        }
    ],

    update: [
        upload.single('image'),
        check('id').custom(value => {
            return courses.findOne({
                _id: value
            }).then(result => {
                if(!result) {
                    throw new Error('Course not found!')
                }
            })
        }),

        (req, res, next) => {
            const errors = validationResult(req);
            if(!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                });
            }
            next();
        }
    ],

    delete: [
        check('id').custom(value => {
            return courses.findOne({
                _id: value
            }).then(result => {
                if(!result) {
                    throw new Error('Course not found!')
                }
            })
        }),

        (req, res, next) => {
            const errors = validationResult(req);
            if(!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                });
            }
            next();
        }
    ],

    publish: [
        check('id').custom(value => {
            return courses.findOne({
                _id: value
            }).then(result => {
                if(!result) {
                    throw new Error('Course not found!')
                }
                if(result.isPublished === true) {
                    throw new Error('Course is already published')
                }
            })
        }),

        (req, res, next) => {
            const errors = validationResult(req)
            if(!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],
    
}