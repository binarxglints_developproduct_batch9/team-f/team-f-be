// IMPORT DEPENDENCIES
const { studentTests, studentOptions } = require('../../models')
const {
    check,
    validationResult, 
    matchedData,
    sanitize
} = require('express-validator')

module.exports = {

    // VALIDATOR FOR GET ONE
    create: [
        check('student_test_id').custom(async (value, {req}) => {
            const result = await studentOptions.findOne({
                studentTest: value,
                answer: req.body.option_id,
                question: req.body.question_id
            }) 

            const checkUserToken = await studentTests.findOne({
                _id: value
            })
            if(result) {
                throw new Error(`Student already answer this question`)
            }

            if(checkUserToken.user != req.user._id) {
                throw new Error(`You don't have access to do this`)
                
            }    
        }),

        (req, res, next) => {
            const errors = validationResult(req)
            if(!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }

            // MOVE TO NEXT MIDDLEWARES IF ALL MEETS THE VALIDATION
            next()
        }
    ],

    delete: [
        check('id').custom(value => {
            return studentOptions.findOne({
                _id: value
            }).then(result => {
                if(!result) {
                    throw new Error('student test answer not found!')
                }
            })
        }),

        (req, res, next) => {
            const errors = validationResult(req)
            if(!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }

            // MOVE TO NEXT MIDDLEWARES IF ALL MEETS THE VALIDATION
            next()
        }
    ]
}