const { courses, lessons } = require('../../models')
const {
    check,
    validationResult, 
    matchedData,
    sanitize
} = require('express-validator')
const multer = require('multer')
const path = require('path')
const crypto = require('crypto')

const uploadDir = '/lessonAssets/'
const storage = multer.diskStorage({
    destination: './public' + uploadDir,
    filename: function(req, file, cb) {
        cb(null, `${Date.now()}-${file.originalname}`)
    }
})

const upload = multer({
    storage: storage,
    dest: uploadDir
})

module.exports = {

    getOne: [
        check('id').custom(value => {
            return lessons.findOne({
                _id: value
            }).then(result => {
                if(!result) {
                    throw new Error('Lesson not found!')
                }
            })
        }),

        (req, res, next) => {
            const errors = validationResult(req)
            if(!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],

    getByCourse: [
        check('id').custom(value => {
            return courses.findOne({
                _id: value
            }).then(result => {
                if(!result) {
                    throw new Error('course not found!')
                }
            })
        }),

        (req, res, next) => {
            const errors = validationResult(req)
            if(!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],

    create: [
        upload.fields([
            {
            name:'video'  
            }, {
            name: 'material'
            }
        ]),
        
        check('description', 'description must be a string').isString(),
        check('title', 'title must be a string').isString().custom(async(value, {req}) => {

            if(!value.includes('Lesson #')) throw new Error(`title must start with "Lesson #"`)
        }),

        check('course_id').custom(async(value, {req}) => {
            const checkCourse = await courses.findOne({
                _id: value
            })
            if(!checkCourse) throw new Error('Course not found!')

            const checkTeacher = await courses.findOne({
                _id: value
            })
            if (checkTeacher.user != req.user._id) throw new Error(`you don't have access to do this`)
        }),

        (req, res, next) => {
            if(req.files.video) {
                const getExtention = req.files.video[0].filename
                const getSize = req.files.video[0].size        
                
                if(!getExtention.includes('.mp4')) {
                    return res.status(422).json({
                        errors: 'Max. size 200 MB. Supported format for video is .mp4'
                    });
                }
            
                if(getSize >= 200000000) {
                    return res.status(422).json({
                        errors: 'Max. size 200 MB. Supported format for video is .mp4'
                    });
                }
            } else {
                return res.status(422).json({
                    errors: 'video must be input'
                });
            }

            if(req.files.material) {

                let obj = []
                for(let key of req.files.material){
                if(!key.filename.includes('pdf'))
                obj.push(key.filename);
                }

                if(obj.length !== 0) {
                    return res.status(422).json({
                        errors: 'Supported format for material is .pdf'
                    });
                }

                let totalSize = []
                for(let key of req.files.material){
                totalSize.push(key.size);
                }

                const reducer = (accumulator, currentValue) => accumulator + currentValue;
                if(totalSize.reduce(reducer) >= 20000000) {
                    return res.status(422).json({
                        errors: 'Max total size of material is 20 MB'
                    });
                }
            } else {
                return res.status(422).json({
                    errors: 'material must be input'
                });
            }

            const errors = validationResult(req)
            if(!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],

    update: [
        upload.fields([
            {
            name:'video'  
            }, {
            name: 'material'
            }
        ]),

        check('id').custom(value => {
            return lessons.findOne({
                _id: value
            }).then(result => {
                if(!result) {
                    throw new Error('Lesson not found!')
                }
            })
        }),

        (req, res, next) => {
            if(req.files.video) {
                const getExtention = req.files.video[0].filename
                const getSize = req.files.video[0].size  

                if(!getExtention.includes('.mp4')) {
                    return res.status(422).json({
                        errors: 'Supported format for video is .mp4'
                    });
                }
           
                if(getSize >= 200000000) {
                    return res.status(422).json({
                        errors: 'Max. size 200 MB. Supported format for video is .mp4'
                    });
                }
            }


            if(req.files.material) {
                let obj = []
                for(let key of req.files.material){
                    if(!key.filename.includes('pdf'))
                    obj.push(key.filename);
                }

                if(obj.length !== 0) {
                    return res.status(422).json({
                        errors: 'Supported format for material is .pdf'
                    });
                }

                let totalSize = []
                for(let key of req.files.material){
                totalSize.push(key.size);
                }

                const reducer = (accumulator, currentValue) => accumulator + currentValue;
                if(totalSize.reduce(reducer) >= 20000000) {
                    return res.status(422).json({
                        errors: 'Max total size of material is 20 MB'
                });
            }
            }
            
            const errors = validationResult(req)
            if(!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],

    delete: [
        check('id').custom(value => {
            return lessons.findOne({
                _id: value
            }).then(result => {
                if(!result) {
                    throw new Error('Lesson not found!')
                }
            })
        }),

        (req, res, next) => {
            const errors = validationResult(req)
            if(!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],

    
}