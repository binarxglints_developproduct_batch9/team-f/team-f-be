// IMPORT DEPENDENCIES
const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const cors = require('cors')

//CORS
app.use(cors())

// IMPORT ROUTES HERE
const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes');
const lessonRoutes = require('./routes/lessonRoutes')
const enrollRoutes = require('./routes/enrollRoutes');
const testRoutes = require('./routes/testRoutes');
const questionRoutes = require('./routes/questionRoutes');
const studentLessonRoutes = require('./routes/studentLessonRoutes');
const optionRoutes = require('./routes/optionRoutes')
const studentTestRoutes = require('./routes/studentTestRoutes')

// SET BODY PARSER FOR HTTP POST OPERATION
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

// SET STATIC ASSET TO PUBLIC DIRECTORY
app.use(express.static('public'));


// ENDPOINT ROUTES
app.use('/', userRoutes);
app.use('/courses', courseRoutes);
app.use('/lessons', lessonRoutes);
app.use('/enrolls', enrollRoutes);
app.use('/tests', testRoutes);
app.use('/questions', questionRoutes);
app.use('/studentLessons', studentLessonRoutes);
app.use('/options', optionRoutes)
app.use('/studentTests', studentTestRoutes)

// PORT
app.listen(3000, () => console.log("LEKTUR Running on port:3000"))

module.exports = app;